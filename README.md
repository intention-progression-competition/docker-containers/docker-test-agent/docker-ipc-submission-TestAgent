You can use this containerised version of the test-agent as an example of how to setup your Dockerfile.

To run please follow the tutorials from https://gitlab.com/intention-progression-competition/docker-containers/docker-ipc/docker-ipc

